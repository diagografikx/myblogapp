<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List
href="Top%2010%20Trending%20Ghana%20songs%20of%202021_files/filelist.xml">
<link rel=Edit-Time-Data
href="Top%2010%20Trending%20Ghana%20songs%20of%202021_files/editdata.mso">
</head>

<body lang=EN-US link=blue vlink="#954F72" style='tab-interval:.5in'>

<div class=WordSection1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:1'><b style='mso-bidi-font-weight:normal'><span
style='font-size:24.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman";mso-font-kerning:18.0pt;mso-no-proof:yes'><!--[if gte vml 1]><v:shapetype
 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_i1025" type="#_x0000_t75"
 alt="Amerado Fr3 Me ft Benerl www crateshub com mp3 image" style='width:531pt;
 height:531pt;visibility:visible;mso-wrap-style:square'>
 <v:imagedata src="https://www.ghupload.com/wp-content/uploads/2021/10/Amerado-Fr3-Me-ft-Benerl-www-crateshub-com_-mp3-image.jpg"
  o:title="Amerado Fr3 Me ft Benerl www crateshub com mp3 image"/>
</v:shape><![endif]--><![if !vml]><img width=708 height=708
src="https://www.ghupload.com/wp-content/uploads/2021/10/Amerado-Fr3-Me-ft-Benerl-www-crateshub-com_-mp3-image.jpg"
alt="Amerado Fr3 Me ft Benerl www crateshub com mp3 image" v:shapes="Picture_x0020_1"><![endif]></span></b><b><span
style='font-size:24.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman";mso-font-kerning:18.0pt'><o:p></o:p></span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:1'><b><span style='font-size:24.0pt;
font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-font-kerning:18.0pt'>Top 10 Trending Ghana songs of 2021<o:p></o:p></span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Half of the year 2021 is gone. This
means you should be halfway through your goals for this year<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Now, onto the main topic. This year
has been exceptional for many new and mainstream Ghanaian musicians. From
reggae/dancehall, <span class=SpellE>afrobeats</span>/afro-pop, highlife, <span
class=SpellE>hiplife</span>/hip-hop to gospel music, hundreds of songs have
been churned out this year. Music charts in local and new media have been busy
switching from old to new sounds.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Many artistes can boast of many
hits, thanks to digital platforms like <span class=SpellE>TikTok</span>,
Instagram, Snapchat and YouTube. Since the pandemic, digital platforms have
been the go-to place for new sounds. It has also harnessed how music is
consumed. In a nutshell, the internet has produced hits more than the local
media.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Its been <span class=SpellE>Kuami</span>
Eugene, <span class=SpellE>KiDi</span> and <span class=SpellE>Mr</span> Drews
year so far. Their records and features blew bigger than many thought.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Below, see 10 Ghanaian songs that
dominated charts, ruled <span class=SpellE>TikTok</span>, Instagram, YouTube
and Apple Music, and shot many new talents to fame.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><i><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>NB: This list has not been ranked.</span></i><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>1. <a
href="https://www.ghupload.com/dollar-on-you-by-kuami-eugene/"><span
class=SpellE>Kuami</span> Eugene - Dollar <span class=GramE>On</span> You</a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>The Lynx Entertainment label star
kicked off the year with a gospel-like song, titled Amen, but Dollar <span
class=GramE>On</span> You took his brand, voice and career a step further. The
<span class=SpellE>afrobeats</span> song is accompanied by a star-studded
visual featuring cameo appearances from Giovani Caleb, <span class=SpellE>Dr</span>
Pounds, <span class=SpellE>Jackline</span> Mensah and more.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>2. <a
href="https://www.ghupload.com/kwame-yogot-biibi-besi-ft-kuami-eugene/">Kwame <span
class=SpellE>Yogot</span> - <span class=SpellE>Biibi</span> <span class=SpellE>Besi</span>
feat <span class=SpellE>Kuami</span> Eugene</a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>A follow up to his breakthrough
song, Bitter Sweet, Kwame <span class=SpellE>Yogot</span> made the right
choice when attracted <span class=SpellE>Kuami</span> Eugenes magic and
influence for this song. Its a mid-tempo <span class=SpellE>afrobeats</span>
jam that was created for party, party, party and everything party.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>3. <a
href="https://www.ghupload.com/kweku-darlington-sika-aba-fie-ft-yaw-tog-kweku-flick/"><span
class=SpellE>Kweku</span> Darlington - <span class=SpellE>Sika</span> Aba Fie
feat. Yaw Tog &amp; <span class=SpellE>Kweku</span> Flick</a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>The <span class=SpellE>Kumerican</span>
star saw his breakthrough this year with <span class=SpellE>Sika</span> Aba
Fie after pushing hard for years. Last year, he released a couple of notable
songs but not until he got Yaw Tog and <span class=SpellE>Kweku</span> Flicks
blessings for this instant hit. The hip-hop song dedicated to hustlers already
has a remix that features <span class=SpellE>Kuami</span> Eugene.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>4. <a
href="https://www.ghupload.com/touch-it-by-kidi/"><span class=SpellE>KiDi</span>
- Touch It</a></span></b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>The Lynx Entertainments star
started the year on a good note, with a collaboration with Nigerian star <span
class=SpellE>Patoranking</span> and label mate <span class=SpellE>Kuami</span>
Eugene for the song Spiritual. But he broke through the international market
once again with Touch It, which is currently <span class=SpellE>TikToks</span>
<span class=SpellE>favourite</span> song.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>5. <a
href="https://www.ghupload.com/okyeame-kwame-yeeko-ft-kuami-eugene/"><span
class=SpellE>Okyeame</span> Kwame - <span class=SpellE>Yeeko</span> feat <span
class=SpellE>Kuami</span> Eugene</a></span></b><span style='font-size:12.0pt;
font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span class=SpellE><span style='font-size:12.0pt;
font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>Kuami</span></span><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'> Eugene has the Midas touch. <span class=SpellE>Okyeame</span>
Kwames comeback song is just one of the many instances. In an exclusive
interview with Pulse.com.gh this year, the Rap Doctor (as popularly known)
disclosed that <span class=SpellE>Kuami</span> Eugene basically did everything
on the song, from writing, arrangement, to production.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>6. <a
href="https://www.ghupload.com/asempa-by-joe-mettle/">Joe Mettle - Ye <span
class=SpellE>Bua</span> <span class=SpellE>Mi</span> (Help Me)</a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>This is the only gospel song on the
list because it is unquestionably the biggest gospel song released in Ghana
this year. The slow-tempo jam which was written in both Ga and English is one
of Joe Mettles best projects so far.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>7. <a
href="https://www.ghupload.com/sefa-e-choke-ft-mr-drew/"><span class=SpellE>Sefa</span>
- E Choke feat <span class=SpellE>Mr</span> Drew</a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>It is the Black Avenue <span
class=SpellE>Muzik</span> biggest moment in her career so far after toiling for
years to find a hit. She has put in a lot of effort and has pushed some good
songs in the last two years but E Choke became an instant hit when she
dropped it.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>8. <a
href="https://www.ghupload.com/slow-down-by-king-promise/">King Promise - Slow
Down</a></span></b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>The <span class=SpellE>Teshie-Nugua</span>
star boy gave the internet a great gift this year. No controversy, no media
gimmicks, but got an instant hit when he dropped the <span class=SpellE>Nonsoamadi</span>
and <span class=SpellE>Killbeatz</span>-produced <span class=SpellE>afrobeats</span>
jam.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'><a
href="https://www.ghupload.com/okenneth-agyeiwaa-ft-reggie-city-boy/"><b>9. <span
class=SpellE>O'kenneth</span> - <span class=SpellE>Agyeiwaa</span> feat. Reggie
&amp; City Boy</b></a><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Trolls attempted to kill <span
class=SpellE>Agyeiwaa</span> but little did they know that it would become <span
class=SpellE>TikTok</span> and Instagram Reels <span class=SpellE>favourite</span>.
The numbers prove that nothing can stop greatness.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>10. <a
href="https://www.ghupload.com/mr-drew-mood/"><span class=SpellE>Mr</span> Drew
- Mood</a></span></b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>The Michael Jackson of our time gave
us something to dance to. Mood is a masterpiece, perfectly created to move
feet and bump heads when its blasted in loudspeakers.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman"'>Source:<a
href="https://www.ghupload.com/"> <span class=SpellE>Ghupload</span></a></span></b><span
style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

</body>

</html>
